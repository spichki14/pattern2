﻿using System;
using System.Collections.Generic;

namespace PATTERN11
{
    // Интерфейс для всех танков
    public interface Tank
    {
        
        void Move();
        void Fire();
        void Execute(ICommand command);

    }
    

    // Конкретные классы танков
    public class LightTank: Tank
    {
        public void Move()
        {
            Console.WriteLine("Light tank is moving");
        }

        public void Fire()
        {
            Console.WriteLine("Light tank is firing");
        }

        public void Execute(ICommand command)
        {
            command.Execute();
        }
    }

    public class HeavyTank :Tank
    {
        public void Move()
        {
            Console.WriteLine("Heavy tank is moving");
        }

        public void Fire()
        {
            Console.WriteLine("Heavy tank is firing");
        }

        public void Execute(ICommand command)
        {
            command.Execute();
        }
    }

    // Интерфейс команды
    public interface ICommand
    {
        void Execute();
    }

    // Конкретные класы команд
    public class MoveCommand : ICommand
    {
        private Tank _tank;

        public MoveCommand(Tank tank)
        {
            _tank = tank;
        }

        public void Execute()
        {
            _tank.Move();
        }
    }

    public class FireCommand : ICommand
    {
        private Tank _tank;

        public FireCommand(Tank tank)
        {
            _tank = tank;
        }

        public void Execute()
        {
            _tank.Fire();
        }
    }

    // Компоновщик команд
    public class CompositeCommand : ICommand
    {
        private List<ICommand> _commands;

        public CompositeCommand()
        {
            _commands = new List<ICommand>();
        }

        public void AddCommand(ICommand command)
        {
            _commands.Add(command);
        }

        public void Execute()
        {
            foreach (var command in _commands)
            {
                command.Execute();
            }
        }
    }

   

    class Program
    {
        static void Main(string[] args)
        {
            // Создаем танки
            Tank lightTank = new LightTank();
            Tank heavyTank = new HeavyTank();

            // Создаем команды для движения и стрельбы
            ICommand moveLightTankCommand = new MoveCommand(lightTank);
            ICommand fireLightTankCommand = new FireCommand(lightTank);

            ICommand moveHeavyTankCommand = new MoveCommand(heavyTank);
            ICommand fireHeavyTankCommand = new FireCommand(heavyTank);

            // Создаем компоновщик команд и добавляем в него все команды
            CompositeCommand compositeCommand = new CompositeCommand();
            compositeCommand.AddCommand(moveLightTankCommand);
            compositeCommand.AddCommand(fireLightTankCommand);
            compositeCommand.AddCommand(moveHeavyTankCommand);
            compositeCommand.AddCommand(fireHeavyTankCommand);

            // Выполняем все команды с помощью компоновщика
            compositeCommand.Execute();

            Console.ReadLine();
        }
    }

}
